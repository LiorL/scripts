#!/bin/bash
#|| [ "$1"=="$2" ]
compare() {
	ref=$1
	dest=$2
	if [ "$ref" = "$dest" ] || [ "$ref" = "x" ] || [ "$ref" = "X" ] || [ "$ref" = "255" ] || [ "$ref" = "0" ] ; then
		return 1
	fi
	return 0
}

filename="$1"
destIP="$2"
echo "checking if ip $destIP is valid"
D1=$(echo $destIP | awk -F'.' '{print $1}')
D2=$(echo $destIP | awk -F'.' '{print $2}')
D3=$(echo $destIP | awk -F'.' '{print $3}')
D4=$(echo $destIP | awk -F'.' '{print $4}')
while read -r line
do
	refIP=$line
	echo "checking againts reference IP: $refIP"
	R1=$(echo $refIP | awk -F'.' '{print $1}')
	R2=$(echo $refIP | awk -F'.' '{print $2}')
	R3=$(echo $refIP | awk -F'.' '{print $3}')
	R4=$(echo $refIP | awk -F'.' '{print $4}')

sum=0

compare $R1 $D1
sum=`expr $sum + $?`
compare $R2 $D2
sum=`expr $sum + $?`
compare $R3 $D3
sum=`expr $sum + $?`
compare $R4 $D4
sum=`expr $sum + $?`
if [ $sum -eq 4 ] ; then
	return 1
fi

done < "$filename"

return 0


