#!/bin/bash
connectionName=$1
simulate=1

TrueFalse(){
if [ $1 -eq 0 ] ; then 
	echo true
else
	echo false
fi
}


GetIpAdress(){
	ifconfig $connectionName | awk '/inet addr/{print substr($2,6)}'
}

IsValidIpAddress(){
IpAddress=$(GetIpAdress)
if [ "x$IpAddress" = "x" ] ; then
#Ip address doesn't exists 
return 2
fi

sh checkIP.sh banned.txt $IpAddress
result=$?

if [ $result -eq 0 ] ; then
	return 0
fi

return 1
}


result=1000;

while [ $result -ne 0 ]
do
	IsValidIpAddress
	result=$?
	echo Valid IP adress: $(TrueFalse $result)
	if [ $result -eq 2 ] ; then
		echo Illegal IP address
		echo sleeping for 60 seconds...
		sleep 60
	fi
	
	if [ $result -eq 1 ] ; then
		if [ $simulate -eq 0 ] ; then
			echo restarting $connectionName connection...
			ifconfig $connectionName down 
		else
			echo Simulating restart of $connectionName connection.
		fi
	
		echo sleeping for 15 seconds..
		sleep 15
	fi
done
